$(document).ready(function() {
	$(document).on('click', '.voted-lol, .voted-smile, .voted-cry, .voted-sad',
  	function() {
  	vote_type = $(this).attr('vote_type');
  	post_id = $('.post-id').val();
    user_id = $('.user-id').val();
    if ($(this).hasClass('actived-vote')){
    	$.ajax({
      	url: '/posts/' + post_id + '/votes',
        type: 'DELETE',
        data: 'user_id=' + user_id,
        success: function(){
        	$('.vote-btn').removeClass('actived-vote');
        	$('.count-'+vote_type).html(parseInt($('.count-'+vote_type).html()) - 1);
        }
      });
    }	else {
  		$.ajax({
      	url: '/posts/' + post_id + '/votes',
        type: 'POST',
        data: 'id=' + post_id + '&vote_types=' + vote_type,
        success: function() {
        	if ($('.vote-btn').hasClass('actived-vote')) {
          	delete_vote = $('.actived-vote').attr('vote_type');
          	$('.count-' + delete_vote).html(parseInt($('.count-' + delete_vote).html()) - 1)
          }
          $('.count-' + vote_type).html(parseInt($('.count-' + vote_type).html()) + 1)
          $('.vote-btn').removeClass('actived-vote');
          $('.voted-' + vote_type).addClass('actived-vote');
        }
      });
  	}
	});
});