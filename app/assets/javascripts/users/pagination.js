$(document).on('turbolinks:load', function () {
  $(window).on('scroll', function(){
    more_posts_url = $('.pagination .next_page').attr('href');
    if (more_posts_url && $(window).scrollTop() > $(document).height() - $(window).height() - 1) {
      $('.pagination').html('<img src="/assets/ajax-loader.gif" alt="Loading..." title="Loading..." />');
      $.getScript(more_posts_url);
    }
  });

	$('.scroll-menu').mousewheel(function(event, delta) {
	  this.scrollLeft -= delta * 30;
	  event.preventDefault();
	});
});
