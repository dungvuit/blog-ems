$(document).ready(function() {
	$(document).on('click', '.edit-name', function(){
		id = $('.input-name-user').attr('data_id');
		$('#input-name-user_'+id).show();
		$('#cancel-name_'+id).show();
		$('#save-name_'+id).show();
		$(this).hide();
		$('#show-name-user_'+id).hide();
	});

	$(document).on('click', '.edit-email', function(){
		id = $('.input-email-user').attr('data_id');
		$('#input-email-user_'+id).show();
		$('#cancel-email_'+id).show();
		$('#save-email_'+id).show();
		$(this).hide();
		$('#show-email-user_'+id).hide();
	});

	$(document).on('click', '.cancel-name', function(){
		id = $('.input-name-user').attr('data_id');
		$('#input-name-user_'+id).hide();
		$(this).hide();
		$('#save-name_'+id).hide();
		$('#edit-name_'+id).show();
		$('#show-name-user_'+id).show();
	});

	$(document).on('click', '.cancel-email', function(){
		id = $('.input-email-user').attr('data_id');
		$('#input-email-user_'+id).hide();
		$(this).hide();
		$('#save-email_'+id).hide();
		$('#edit-email_'+id).show();
		$('#show-email-user_'+id).show();
	});

	$(document).on('click', '.save-name', function(){
		id = $('.input-name-user').attr('data_id');
		val_name = $('.input-name-user').val();
		$.ajax({
			url: '/users/update_name',
			method: 'PATCH',
			data: 'id=' + id + '&name=' + val_name,
			success: function(){
				$('#input-name-user_'+id).hide();
				$('#cancel-name_'+id).hide();
				$('#save-name_'+id).hide();
				$('#edit-name_'+id).show();
				$('#show-name-user_'+id).show();
				$('#show-name-user_'+id).text(val_name);
			}
		});
	});

	$(document).on('click', '.save-email', function(){
		id = $('.input-email-user').attr('data_id');
		val_email = $('.input-email-user').val();
		$.ajax({
			url: '/users/update_email',
			method: 'PATCH',
			data: 'id=' + id + '&email=' + val_email,
			success: function(){
				$('#input-email-user_'+id).hide();
				$('#cancel-email_'+id).hide();
				$('#save-email_'+id).hide();
				$('#edit-email_'+id).show();
				$('#show-email-user_'+id).show();
				$('#show-email-user_'+id).text(val_email);
			}
		});
	});
});