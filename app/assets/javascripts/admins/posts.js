function readFile(input, preview) {
  if (input.files && input.files[0]) {
    reader = new FileReader;
    reader.onload = function(e) {
      $(preview).attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
  }
};

function choosedate(){
  $('.time-public-update').datetimepicker({
    format: 'YYYY-MM-DD HH:mm',
    sideBySide: true
  });
}

$(document).on('turbolinks:load', function() {
  $(document).on('change', '.upload-thumbnail', function() {
    readFile(this, $('#thumbnail-view'));
  });

  $(document).on('change', '.upload-avatar', function() {
    readFile(this, $('#avatar-view'));
  });

  if ($('.upload-thumbnail').length && $('.upload-thumbnail').val().length) {
    readFile($('.upload-thumbnail').get(0), $('#thumbnail-view'));
  }

  if ($('.upload-avatar').length && $('.upload-avatar').val().length) {
    readFile($('.upload-avatar').get(0), $('#avatar-view'));
  }

  $(document).on('click', '#preview-btn', function() {
    $('#content-preview').html(CKEDITOR.instances.post_content.getData());
  });

  $(document).on('change', '#category, #publish, #suggest, #publish_time', function() {
    $('.custom-btn').trigger('click');
  });

  $('#time-public-update').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss',
    sideBySide: true
  });

  $('.time-public-update').datetimepicker({
    format: 'YYYY-MM-DD HH:mm',
    sideBySide: true
  });
});

