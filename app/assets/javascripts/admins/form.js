$(document).ready(function() {
	$(document).on('click', '#btn-submit', function(){
    checkValue('#post_title', '#title-error', "Title can't blank!");
    checkValue('#time-public-update', '#publish-time-error', "Time Public can't blank!");
    checkValueImage('.image-create','post_thumbnail', '#thumbnail-error', "Thumbnail can't blank!");
    checkImageFormat('post_thumbnail', '#thumbnail-error', "Just upload format JPG, PNG");
    checkValueContent('#content-error', "Content can't blank!");
    checkValue('#post_author_name', '#author-name-error', "Author name can't blank!");
    checkValue('#post_position', '#position-error', "position can't blank!");
    checkValue('#post_age', '#age-error', "age can't blank!");
    // checkValueImage('.image-create','post_avatar', '#avatar-error', "avatar can't blank!");
    checkImageFormat('post_avatar', '#avatar-error', "Just upload format JPG, PNG");

    backToTop();

    if (checkValue('#post_title', '#title-error', "Title can't blank!") ||
      checkValue('#time-public-update', '#publish-time-error', "Time Public can't blank!") ||
      checkValueImage('.image-create','post_thumbnail', '#thumbnail-error', "Thumbnail can't blank!") ||
      checkImageFormat('post_thumbnail', '#thumbnail-error', "Just upload format JPG, PNG") ||
      checkValue('#post_author_name', '#author-name-error', "Author name can't blank!") ||
      checkValueContent('#content-error', "Content can't blank!") ||
      checkValue('#post_position', '#position-error', "position can't blank!") ||
      checkValue('#post_age', '#age-error', "age can't blank!") ||
      // checkValueImage('.image-create','post_avatar', '#avatar-error', "avatar can't blank!") ||
      checkImageFormat('post_avatar', '#avatar-error', "Just upload format JPG, PNG")) {
      return false;
    }

    hideInput();
	});

	$(document).on('click', '#btn-cancel', function(){
    showInput();
	});

	$(document).on('click', '#btn-confirm', function(){
		$('.btn-save').trigger('click');
	});

  function checkValue(input, error, message){
    var value = $(input).val();
    if (value === "") {
      $(error).html(message);
      return true;
    }
  }

  function checkValueContent(error, message) {
    if (CKEDITOR.instances.post_content.getData() === ""){
      $(error).html(message);
      return true;
    } else {
      $(error).html('');
    }
  }

  function checkImageFormat(dom_input, error, message){
    var input = document.getElementById(dom_input);
    var allowType = /(\.jpg|\.png)$/i;
    if(input.files.length > 0){
      if (!allowType.exec(input.files[0].name)){
        $(error).html(message);
        return true;
      }
    }
  }

  function checkValueImage(dom_create, dom_input, error, message_require){
    var input = document.getElementById(dom_input);
    if(($(dom_create).length > 0) && (input.files.length === 0)){
      $(error).html(message_require);
      return true;
    }
  }

  function checkValueOnChange(dom_input, error, message){
    var value = $(dom_input).val();
    $(error).html('');
    if (value === '') {
      $(error).html(message);
    }
  }

  function checkFormatAge(dom_input, error, message, error_format, message_format, error_number, message_number){
    var value = $(dom_input).val();
    $(error).html('');
    $(error_format).html('');
    $(error_number).html('');
    if (value === '') {
      $(error).html(message);
    } else if (value != parseInt(value)) {
      $(error_format).html(message_format);
    } else if (parseInt(value) < 25 || parseInt(value) > 50) {
      $(error_number).html(message_number);
    }
  }

  $(document).on('change', '#post_title', function(){
    checkValueOnChange('#post_title', '#title-error', "Title can't blank!");
  });

  $(document).on('dp.change', '#time-public-update', function(){
    checkValueOnChange('#time-public-update', '#publish-time-error', "Public time can't blank!");
  });

  $(document).on('change', '#post_author_name', function(){
    checkValueOnChange('#post_author_name', '#author-name-error', "Author name can't blank!");
  });

  $(document).on('change', '#post_position', function(){
    checkValueOnChange('#post_position', '#position-error', "Position can't blank!");
  });

  $(document).on('change', '#post_age', function(){
    checkFormatAge('#post_age', '#age-error', "Age can't blank!", '#age-error-format', "Age must is number", '#age-error-number', "Age author must more than 25 and less than 50");
  });

  $(document).on('change', '.upload-thumbnail', function(){
    $('#thumbnail-error').html('');
  });

  $(document).on('change', '.upload-avatar', function(){
    $('#avatar-error').html('');
  });

  function hideInput(){
    //title
    $('#post_title').addClass('hidden');
    $('#title-confirm').html($('#post_title').val());
    //category
    $('#post_category').addClass('hidden');
    $('#category-confirm').html($('#post_category').val());
    //publish-time
    $('#time-public-update').addClass('hidden');
    $('#publish-time-confirm').html($('#time-public-update').val());
    //Checkbox
    checkboxConfirm($('#post_publish'), 'post_publish', $('#publish-confirm'), 'public', 'not public');
    checkboxConfirm($('#post_suggest'), 'post_suggest', $('#suggest-confirm'), 'suggest', 'not suggest');
    //thumbnail
    $('#input-thumbnail').addClass('hidden');
    //content
    $('#body-content').addClass('hidden');
    $('#content-confirm').html(CKEDITOR.instances.post_content.getData());
    //author name
    $('#post_author_name').addClass('hidden');
    $('#author-name-confirm').html($('#post_author_name').val());
    //position
    $('#post_position').addClass('hidden');
    $('#position-confirm').html($('#post_position').val());
    //age
    $('#post_age').addClass('hidden');
    $('#age-confirm').html($('#post_age').val());
    //avatar
    $('#input-avatar').addClass('hidden');

    $('.before-confirm').addClass('hidden');
    $('.after-confirm').removeClass('hidden');
  }

  function showInput(){
    //title
    showValue('#post_title', '#title-confirm');
    //category
    showValue('#post_category', '#category-confirm');
    //publish-time
    showValue('#time-public-update', '#publish-time-confirm');
    //Checkbox
    showValue('#post_publish', '#publish-confirm');
    showValue('#post_suggest', '#suggest-confirm');
    //thumbnail
    $('#input-thumbnail').removeClass('hidden');
    //content
    showValue('#body-content', '#content-confirm');
    //author name
    showValue('#post_author_name', '#author-name-confirm');
    //position
    showValue('#post_position', '#position-confirm');
    //age
    showValue('#post_age', '#age-confirm');
    //avatar
    $('#input-avatar').removeClass('hidden');

    $('.after-confirm').addClass('hidden');
    $('.before-confirm').removeClass('hidden');
  }

  function showValue(dom_input, dom_show){
    $(dom_input).removeClass('hidden');
    $(dom_show).html('');
  }

  function checkboxConfirm(checkbox_tag, id, confirm, text1, text2){
    $(checkbox_tag).addClass('hidden');
    var checkbox = document.getElementById(id)
    if (checkbox.checked === true){
      $(confirm).html(text1);
    } else {
      $(confirm).html(text2)
    }
  }

  function backToTop(){
    $('html,body').animate({
      scrollTop: 0
    }, 500);
  }
});
