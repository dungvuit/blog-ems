$(document).ready(function() {
  $(document).on('click', '.set-private', function() {
    var id;
    id = $(this).attr('data_id');
    $.ajax({
      url: '/admins/posts/update_publish',
      type: 'PATCH',
      data: 'id=' + id,
      dataType: 'script',
      success: function(){
        $('#set-private_' + id).remove();
        $('#set-status_' + id).html('Not Publish');
      },
    });
  });

  $(document).on('click', '.edit-time-public', function() {
    id = $(this).attr('data_id');
    $('#update-time-public_' + id).show();
    $('#cancel-time-public_' + id).show();
    $('#input-time-public-update_' + id).show();
    $('#time-public_' + id).hide();
    $(this).hide();
    $('#set-private_' + id).hide();
  });
  $(document).on('click', '.cancel-time-public', function() {
    id = $(this).attr('data_id');
    $('#update-time-public_' + id).hide();
    $('#cancel-time-public_' + id).hide();
    $('#input-time-public-update_' + id).hide();
    $('#time-public_' + id).show();
    $('#edit-time-public_' + id).show();
    $('#set-private_' + id).show();
  });
  $(document).on('click', '.update-time-public', function() {
    id = $(this).attr('data_id');
    publishtime = $('#input-time-public-update_' + id).val();
    $.ajax({
      url: '/admins/posts/update_timepublish',
      type: 'PATCH',
      data: 'id=' + id + '&publish_time=' + publishtime,
      dataType: 'script',
      success: function(){
        $('#update-time-public_' + id).hide();
        $('#cancel-time-public_' + id).hide();
        $('#input-time-public-update_' + id).hide();
        $('#edit-time-public_' + id).show();
        $('#set-private_' + id).show();
        $('#time-public_' + id).show();
        $('#time-public_' + id).html(publishtime);
      },
    });
  });
});