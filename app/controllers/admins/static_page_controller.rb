module Admins
  class StaticPageController < ApplicationController
    before_action :authenticate_admin!
    def home; end
  end
end
