module Admins
  class PostsController < ApplicationController
    layout 'application'
    include PostsHelper

    before_action :authenticate_admin!
    before_action :find_post, except: %i[index new create]
    before_action :load_category, only: %i[index new edit]

    def index
      @posts = Post.filter_post params
      respond_to do |format|
        format.html
        format.js
      end
    end

    def new
      @post = Post.new
    end

    def create
      @post = Post.new blog_params
      if @post.save
        redirect_to admins_posts_path
        flash[:success] = 'Post was successfully created.'
      else
        load_category
        render :new
      end
    end

    def edit; end

    def update
      if @post.update_attributes blog_params
        redirect_to admins_posts_path
        flash[:success] = 'Post was successfully updated.'
      else
        load_category
        render :edit
      end
    end

    def update_publish
      @post.update_attributes(publish: false)
    end

    def update_timepublish
      @post.update_attributes(publish_time: params[:publish_time])
      respond_to do |format|
        format.js { @post }
      end
    end

    private

    def find_post
      @post = Post.find_by id: params[:id]
    end

    def blog_params
      params.require(:post).permit(
        :title, :category, :thumbnail, :content, :publish,
        :suggest, :publish_time, :avatar, :author_name, :position, :age,
        :avatar
      )
    end

    def load_category
      @category = Post.categories.keys
    end
  end
end
