class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  layout :set_layout
  before_action :set_cookies

  def set_layout
    if devise_controller? && resource_name == :user
      'users'
    else
      'application'
    end
  end

  def after_sign_in_path_for(_resource)
    if current_admin
      admins_root_path
    else
      root_path
    end
  end

  def after_sign_out_path_for(_resource)
    if resource_name == :user
      new_user_session_path
    elsif resource_name == :admin
      new_admin_session_path
    else
      root_path
    end
  end

  def set_cookies
    if cookies[:guest_vote].blank?
      cookies[:guest_vote] = {
        value: Random.new_seed,
        expires: 1.year.from_now
      }
    else
      cookies[:guest_vote]
    end
  end
end
