class CreatorsController < ApplicationController
  layout 'users'
  before_action :authenticate_user!

  def index
    @posts = Post.all
  end
end
