class UsersController < ApplicationController
  layout 'users'
  before_action :authenticate_user!

  before_action :find_user, except: %i[index new create]
  before_action :find_post, except: %i[index new create]

  def index
    @users = User.all
  end

  def update_avatar
    @user.update_attributes avatar: params[:avatar]
  end

  def update_name
    @user.update_attributes name: params[:name]
  end

  def update_email
    @user.update_attributes email: params[:email]
  end

  private

  def find_user
    @user = User.find_by id: params[:id]
  end

  def find_post
    @post = Post.find_by id: params[:id]
  end
end
