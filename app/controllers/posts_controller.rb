class PostsController < ApplicationController
  layout 'users'
  before_action :authenticate_user!

  before_action :find_post, except: %i[index new create]
  before_action :find_vote, except: %i[index new create]

  def index
    @load_more = params[:load_more]
    @posts = Post.filter_index params
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    redirect_to posts_url unless @post.publish
  end

  def author; end

  private

  def find_post
    @post = Post.find_by id: params[:id]
    return if @post
    redirect_to posts_url
  end

  def find_vote
    @vote = if user_signed_in?
              @post.votes.find_by user_id: current_user.id
            else
              @post.votes.find_by guest_vote: cookies[:guest_vote]
            end
  end
end
