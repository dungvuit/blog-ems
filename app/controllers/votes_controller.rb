class VotesController < ApplicationController
  layout 'users'
  before_action :authenticate_user!
  before_action :find_post, only: %i[create]

  def create
    @vote = @post.votes.build vote_types: params[:vote_types],
      user_id: find_user, guest_vote: cookies[:guest_vote]
    return unless @vote.save
  end

  def destroy
    @vote = if current_user.present?
              Vote.find_by(params[:post_id], params[:user_id])
            else
              Vote.find_by(params[:post_id], cookies[:guest_vote])
            end
    @vote.destroy
  end

  private

  def find_post
    @post = Post.find_by id: params[:id]
  end

  def find_user
    return unless user_signed_in?
    current_user.id
  end
end
