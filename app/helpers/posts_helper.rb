module PostsHelper
  def default_image(post)
    if post.thumbnail.present?
      image_tag post.thumbnail, size: Settings.thumbnail_size_index
    else
      image_tag 'default.png', size: Settings.thumbnail_size_index
    end
  end

  def default_avatar(post)
    if post.avatar.present?
      image_tag post.avatar, size: Settings.thumbnail_size_index
    else
      image_tag 'background-author.png', size: Settings.thumbnail_size_index
    end
  end

  def avatar_author(post)
    if post.avatar.present?
      image_tag post.avatar
    else
      image_tag 'default.png'
    end
  end

  def open_popup?
    if session[:open_popup].nil?
      session[:open_popup] = true
      return true
    end
    false
  end

  def get_avatar_temp(params)
    path_avatar = "#{Pathname.new Rails.root}/public/#{params[:post][:avatar]}"
    return unless File.exist? path_avatar
    File.open(path_avatar)
  end

  def get_thumbnail_temp(params)
    path_thumbnail = "#{Pathname.new Rails.root}/public/"\
                     "#{params[:post][:thumbnail]}"
    return unless File.exist? path_thumbnail
    File.open(path_thumbnail)
  end
end
