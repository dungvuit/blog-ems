module ApplicationHelper
  def full_title(page_title = '')
    base_title = 'Blog'
    "#{page_title} | #{base_title}" if page_title.present?
  end

  def active_class(link_path)
    current_page?(link_path) ? 'actived' : ''
  end

  def active_class_admin(link_path)
    current_page?(link_path) ? 'active' : ''
  end
end
