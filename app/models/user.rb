class User < ApplicationRecord
  attr_accessor :remote_avatar_url
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: %i[facebook google_oauth2]

  has_many :votes

  class << self
    def new_with_session(params, session)
      super.tap do |user|
        if data == session['devise.facebook_data'] &&
           session['devise.facebook_data']['extra']['raw_info']
          user.email = data['email'] if user.email.blank?
        end
      end
    end

    def from_omniauth(auth)
      where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
        user.email = auth.info.email
        user.password = Devise.friendly_token[0, 20]
        user.name = auth.info.name
        user.remote_avatar_url = auth.info.image
      end
    end
  end
end
