class Vote < ApplicationRecord
  belongs_to :post

  before_save :destroy_vote

  enum vote_types: %i[lol smile sad cry]

  def destroy_vote
    vote = if user_id.present?
             Vote.find_by(user_id: user_id, post_id: post_id)
           else
             Vote.find_by(guest_vote: guest_vote, post_id: post_id)
           end
    vote.destroy if vote.present?
  end
end
