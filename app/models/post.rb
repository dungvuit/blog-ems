class Post < ApplicationRecord
  scope :search_title, ->(search) {
    where 'title LIKE ?', "%#{search}%" if search.present?
  }

  scope :search_category, ->(search) {
    where category: search if search.present?
  }

  scope :search_public_unpublic, ->(search) {
    where(publish: search == 'true') if search.present?
  }

  scope :search_suggest_unsuggest, ->(search) {
    where(suggest: search == 'true') if search.present?
  }

  scope :sort_time_public_asc, ->(search) {
    order publish_time: search == 'true' ? :desc : :asc if search.present?
  }

  scope :filter_all, (lambda do |params|
    search_title(params[:post_title]).search_category(params[:category])
                                     .search_public_unpublic(
                                       params[:publish]
                                     )
                                     .search_suggest_unsuggest(
                                       params[:suggest]
                                     )
                                     .sort_time_public_asc(
                                       params[:publish_time]
                                     )
  end)

  scope :suggest, -> { where suggest: true }
  scope :unsuggest, -> { where suggest: false }

  scope :set_current_time, -> { where('publish_time < ?', DateTime.now) }

  mount_uploader :thumbnail, ImageUploader
  mount_uploader :avatar, ImageUploader
  enum category: %i[study school family]

  has_many :votes

  def count_vote(vote_type)
    votes.where(vote_types: vote_type).count
  end

  def voted?(user)
    votes.where(user_id: user.id).present?
  end

  def check_publish_time?
    publish_time > DateTime.now
  end

  class << self
    def filter_post(params)
      Post.filter_all(params)
          .paginate page: params[:page], per_page: 20
    end

    def filter_index(params)
      Post.filter_all(params)
          .search_public_unpublic('true')
          .set_current_time
          .sort_time_public_asc('true')
          .paginate page: params[:page], per_page: 10
    end
  end
end
