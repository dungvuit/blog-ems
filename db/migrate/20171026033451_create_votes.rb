class CreateVotes < ActiveRecord::Migration[5.1]
  def change
    create_table :votes do |t|
      t.integer :user_id
      t.references :post, foreign_key: true
      t.integer :vote_types, default: 0
      t.string :guest_vote
      t.timestamps
    end
  end
end
