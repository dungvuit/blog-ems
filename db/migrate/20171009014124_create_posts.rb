class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :title
      t.integer :category, default: 0
      t.string :thumbnail
      t.text :content, limit: 2.megabytes
      t.boolean :publish, default: true
      t.boolean :suggest, default: true
      t.datetime :publish_time
      t.string :author_name, default: "ナルフォード"
      t.string :position, default: "塾講師"
      t.integer :age, default: 26
      t.string :avatar, default: 'background-author.png'
      
      t.timestamps
    end
  end
end
