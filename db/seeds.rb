Admin.create password: "123456", email: "admin@gmail.com"

100.times do
  title = "記事タイトルが入ります。記事タイトルが入ります。記事タイトルが入ります。記事タイトルが入ります。"
  content = Faker::Lorem.paragraphs
  publish = Faker::Boolean.boolean
  suggest = Faker::Boolean.boolean
  category = Faker::Number.between(0, 2)
  thumbnail = File.open('app/assets/images/post-thumbnail.png')
  publish_time = Time.zone.now
  author_name = "ナルフォード"
  position = "塾講師"
  age = 26
  avatar = File.open('app/assets/images/background-author.png')
  Post.create title: title, publish_time: publish_time, publish: publish,
    category: category, suggest: suggest, content: content, thumbnail: thumbnail,
      author_name: author_name, position: position, age: age, avatar: avatar
end
