Rails.application.routes.draw do
  devise_for :admins, skip: [:passwords, :registrations]
  mount Ckeditor::Engine => '/ckeditor'
  root 'posts#index'
  devise_for :users, skip: [:passwords, :registrations],
    controllers:{omniauth_callbacks: "users/omniauth_callbacks"}
  resources :users do
    collection do
      patch 'update_name'
      patch 'update_email'
    end
  end
  resources :creators
  resources :chats
  resources :posts do
    collection do
      get ':id/author', to: 'posts#author', as: 'post_author'
    end
    resources :votes, except: :delete
    delete 'votes', to: 'votes#destroy'
  end
  namespace :admins do
    root 'static_page#home'
    resources :users
    resources :posts do
      collection do
       patch 'update_publish'
       patch 'update_timepublish'
       post 'preview'
       patch ':id/confirm', to: 'posts#confirm', as: 'confirm'
      end
    end
  end
end
